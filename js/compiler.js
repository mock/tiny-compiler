/**
 * Main compiler class which operates with Lexer and Parser
 * and also makes AST optimization. After that it generates `asm` code
 * for the simulator.
 * @class
 * @constructor
 **/
var Compiler = function  () {};

/**
 * Generates non-optimized AST from the input string.
 * @param {String} [program]
 * @return {Object} AST
 **/
Compiler.prototype.pass1 = function (program) {
  this.lexer = new Lexer();
  this.parser = new Parser(this.lexer);
  return this.parser.parse(program);
};

/**
 * Optimization AST. Resolves all 'imm' operations.
 * @param {Object} [ast] non-optimized AST
 * @returns {Object} optimized AST
 **/
Compiler.prototype.pass2 = function (ast) {
  var temp;

  if (ast.a && ast.b) {
    if (ast.a.op !== 'imm') {
      this.pass2(ast.a);
    }
    if (ast.b.op !== 'imm') {
      this.pass2(ast.b);
    }
    if ((ast.a.op === 'imm') && (ast.b.op === 'imm')) {
      // eval == evil ^^
      switch (ast.op) {
        case '+':
          temp = ast.a.n + ast.b.n;
          break;

        case '-':
          temp = ast.a.n - ast.b.n;
          break;

        case '*':
          temp = ast.a.n * ast.b.n;
          break;

        case '/':
          temp = ast.a.n / ast.b.n;
          break;
      }

      ast.op = 'imm';
      ast.n = temp;
      delete ast.a;
      delete ast.b;
    }
  }
  return ast;
};

/**
 * Generates ASM code for the simulator.
 * @param {Object} [ast] AST
 * @returns {Array} the sequence of commands.
 **/
Compiler.prototype.pass3 = function (ast) {
  var asm = [];
  var stackRoutine = function() {
    asm = asm.concat(Compiler.prototype.pass3(ast.a));
    asm.push('PU');
    asm = asm.concat(Compiler.prototype.pass3(ast.b));
    asm.push('SW');
    asm.push('PO');
  };
  switch (ast.op) {
    case 'imm':
      asm.push('IM ' + ast.n);
      break;

    case 'arg':
      asm.push('AR ' + ast.n);
      break;

    case '+':
      stackRoutine();
      asm.push('AD');
      break;

    case '-':
      stackRoutine();
      asm.push('SU');
      break;

    case '*':
      stackRoutine();
      asm.push('MU');
      break;

    case '/':
      stackRoutine();
      asm.push('DI');
      break;
  }
  return asm;
};

/**
 * Main method to compile code and generate asm.
 * @param {String} [program]
 * @returns {Array} array of asm-instructions.
 **/
Compiler.prototype.compile = function (program) {
  return this.pass3(this.pass2(this.pass1(program)));
};

