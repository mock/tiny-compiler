(function (global) {

  /**
   * Parses the array of tokens and generates AST.
   * @class
   * @param {Lexer} [lexer]
   * @constructor
   */
  var Parser = function (lexer) {
    this.lexer = lexer;
    this.args = [];
  };

  Parser.prototype = {
    constructor: Parser,

    /**
     * List of arguments.
     * @default
     * @type {Array}
     */
    args: null,

    /**
     * Factor.
     * @description
     * factor ::= number
     *            | variable
     *            | '(' expression ')'
     **/
    factor: function () {
      var token = this.lexer.getToken(),
          node,
          value;

      if (token.type === Lexer.TOKEN.VAR) {
        value = this.args.indexOf(token.value);
        if (value < 0) {
          throw new Error('Variable ' + token.value + ' is not defined.');
        }
        node = {
          op: 'arg',
          n : value
        };
        this.lexer.nextToken();
        return node;
      }

      if (token.type === Lexer.TOKEN.NUM) {
        node = {
          op: 'imm',
          n : token.value
        };
        this.lexer.nextToken();
        return node;
      }

      return this.parenExpression();
    },

    /**
     * term ::= factor
     *          | term '*' factor
     *          | term '/' factor
     **/
    term: function () {
      var node = this.factor(),
          token = this.lexer.getToken();

      while (token.type === Lexer.TOKEN.OP1) {
        this.lexer.nextToken();
        node = {
          op: token.value,
          a : node,
          b : this.factor()
        };
        token = this.lexer.getToken();
      }
      return node;
    },

    /**
     * expression ::= term
     *                | expression '+' term
     *                | expression '-' term
     **/
    expression: function () {
      var node = this.term(),
          token = this.lexer.getToken();

      while (token.type === Lexer.TOKEN.OP2) {
        this.lexer.nextToken();
        node = {
          op: token.value,
          a : node,
          b : this.term()
        };
        token = this.lexer.getToken();
      }
      return node;
    },

    /**
     * Expression in parentheses.
     */
    parenExpression: function () {
      var node;

      if (this.lexer.getToken().type !== Lexer.TOKEN.LB) {
        throw new Error('Unexpected identifier. `(` is expected');
      }

      this.lexer.nextToken();

      node = this.expression();

      if (this.lexer.getToken().type !== Lexer.TOKEN.RB) {
        throw new Error('Unexpected identifier. `)` is expected');
      }
      this.lexer.nextToken();
      return node;
    },

    /**
     *  arg-list   ::= / nothing /
     *                |variable arg-list
     */
    argumentsList: function () {
      var args = [];

      if (this.lexer.getToken().type !== Lexer.TOKEN.LAB) {
        throw new Error('Arguments is not defined');
      }

      this.lexer.nextToken();

      while (this.lexer.getToken().type === Lexer.TOKEN.VAR) {
        args.push(this.lexer.getToken().value);
        this.lexer.nextToken();
      }

      if (this.lexer.getToken().type !== Lexer.TOKEN.RAB) {
        throw new Error('Unexpected token. Expected `]`');
      }

      this.lexer.nextToken();

      return args;
    },

    /**
     *  function   ::= '[' arg-list ']' expression
     */
    func: function () {
      this.args = this.argumentsList();
      return this.expression();
    },

    /**
     * Parses the input string and generates AST.
     * @param {String} [program]
     * @returns {Object} AST
     */
    parse: function (program) {
      this.lexer.run(program);
      return this.func();
    }
  };

  // Exporting
  global.Parser = Parser;
})(this);