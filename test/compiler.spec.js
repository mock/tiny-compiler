describe('Testing compiler', function () {
  var compiler;

  beforeEach(function() {
    compiler = new Compiler();
  });

  it('should compile correct asm-code', function() {
    var prog = '[ x y z ] ( 2*3*x + 5*y - 3*z ) / (1 + 3 + 2*2)',
        compiled = compiler.compile(prog);

    expect(compiled).toEqual(["IM 6", "PU", "AR 0", "SW", "PO", "MU", "PU", "IM 5", "PU", "AR 1", "SW", "PO", "MU", "SW", "PO", "AD", "PU", "IM 3", "PU", "AR 2", "SW", "PO", "MU", "SW", "PO", "SU", "PU", "IM 8", "SW", "PO", "DI"]);
  });

  it('should return correct result', function() {
    var prog = '[ x y z ] ( 2*3*x + 5*y - 3*z ) / (1 + 3 + 2*2)',
      compiled = compiler.compile(prog);

    expect(simulate(compiled,[4,0,0])).toEqual(3);
    expect(simulate(compiled,[4,8,0])).toEqual(8);
    expect(simulate(compiled,[4,8,16])).toEqual(2);
  });

  it('should return correct result 2', function() {
    var prog = '[ x y z ] x - y - z + 10 / 5 / 2 - 7 / 1 / 7',
      compiled = compiler.compile(prog);

    expect(simulate(compiled,[4,5,6])).toEqual(-7);
  });

});