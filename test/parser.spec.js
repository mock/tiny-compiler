describe('Testing parser', function () {
  var lexer, parser;

  beforeEach(function() {
    lexer = new Lexer();
    parser = new Parser(lexer);
  });

  it('should generate correct AST', function() {
    var prog = '[ x y z ] ( 2*3*x + 5*y - 3*z ) / (1 + 3 + 2*2)';
    var t1 = {"op":"/","a":{"op":"-","a":{"op":"+","a":{"op":"*","a":{"op":"*","a":{"op":"imm","n":2},"b":{"op":"imm","n":3}},"b":{"op":"arg","n":0}},"b":{"op":"*","a":{"op":"imm","n":5},"b":{"op":"arg","n":1}}},"b":{"op":"*","a":{"op":"imm","n":3},"b":{"op":"arg","n":2}}},"b":{"op":"+","a":{"op":"+","a":{"op":"imm","n":1},"b":{"op":"imm","n":3}},"b":{"op":"*","a":{"op":"imm","n":2},"b":{"op":"imm","n":2}}}};
    expect(parser.parse(prog)).toEqual(t1);
  });
  it('should work correct with empty arguments', function() {
    var prog = '[  ] ( 10 * 20 + 15 ) / 5';
    var t1 = {"op":"/","a":{"op":"+","a":{"op":"*","a":{"op":"imm","n":10},"b":{"op":"imm","n":20}},"b":{"op":"imm","n":15}},"b":{"op":"imm","n":5}};
    expect(parser.parse(prog)).toEqual(t1);
  });
});